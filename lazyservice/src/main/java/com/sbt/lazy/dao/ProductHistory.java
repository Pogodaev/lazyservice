package com.sbt.lazy.dao;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
@Entity
public class ProductHistory {
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Product_id")
    private Product productId;
    private Date operationDate;
    private String operationType;
    private BigDecimal summ;
    public ProductHistory(){

    }
}
