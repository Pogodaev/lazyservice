package com.sbt.lazy.dao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Client {
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private Long client_id;

    @OneToMany(mappedBy = "clientPId",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Product> products= new ArrayList<>();
    @OneToMany(mappedBy = "clientId",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Address> addresses = new ArrayList<>();
    @OneToMany(mappedBy = "client",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<History> histories= new ArrayList<>();
    public Client(){
    }


}
