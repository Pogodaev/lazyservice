package com.sbt.lazy.dao;

import javax.persistence.*;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private Long id;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private Client clientId;
//    private Integer clientId;
    private String street;
    private String city;
    private String house;
    public Address(){}
    public Address(Client clientId,String street,String city,String house){
        this.clientId = clientId;
        this.street = street;
        this.city = city;
        this.house = house;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

}
