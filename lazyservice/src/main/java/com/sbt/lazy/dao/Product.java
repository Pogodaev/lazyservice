 package com.sbt.lazy.dao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long Product_id;
    public Product(){

    }
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id")
    private Client clientPId;//Client
    private String code;
    private Date activationDate;
    @OneToMany(mappedBy = "productId",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<ProductHistory> productHistoryList= new ArrayList<>();
}
