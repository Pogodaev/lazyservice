package com.sbt.lazy.dao;

import javax.persistence.*;
import java.util.Date;
/**
 *История Группа риска */
@Entity
public class History {
    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private Client client;
    private Date beginDt;
    private Date endDt;
    private String riskPower;
    public History(){

    }
}
