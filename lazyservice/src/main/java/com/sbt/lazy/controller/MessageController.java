package com.sbt.lazy.controller;


import com.sbt.lazy.dao.Address;
import com.sbt.lazy.repos.AddressRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("message")
public class MessageController {

    private AddressRepo addressRepo;

    private int counter = 4;
    private List<Map<String, String>> messages = new ArrayList<Map<String, String>>() {{
        add(new HashMap<String, String>() {{
            put("id", "1");
            put("text", "First message");
        }});
        add(new HashMap<String, String>() {{
            put("id", "2");
            put("text", "Second message");
        }});
        add(new HashMap<String, String>() {{
            put("id", "3");
            put("text", "Third message");
        }});
    }};

    @Autowired
    public void setAddressRepo(AddressRepo addressRepo) {
        this.addressRepo = addressRepo;
    }
    //Iterator<T> source = ...;
    //List<T> target = new ArrayList<>();
    //source.forEachRemaining(target::add);
    //Изменить: выше, для Iterator. Если вы имеете дело с Iterable,
    //
    //iterable.forEach(target::add);

    //    public List<Map<String,String>> list(){
    //вывел одну
    @GetMapping
    public List<Address> list() {
        Address address = new Address();
        address.setCity("spb");
        address.setHouse("2");
        address.setStreet("new");
        addressRepo.save(address);
        Iterable<Address> list = addressRepo.findAll();

        List<Address> list1 = new ArrayList<>();

        list.forEach(list1::add);

        return list1;
    }
//    @GetMapping("{id}")
//    public Map<String,String> getOne(@PathVariable String id){
//        return getMessage(id);
//    }
//
//    private Map<String, String> getMessage(@PathVariable String id) {
//        return messages.stream().filter(message->message.get("id")
//                .equals(id)).findFirst()
//                .orElseThrow(NotFoundException::new);
//    }
//
//    @PostMapping
//    public Map<String,String> create(@RequestBody Map<String,String> message)
//    {
//        message.put("id",String.valueOf(counter++));
//        messages.add(message);
//        return message;
//    }
//    @PutMapping("{id}")
//    public Map<String,String> update(@PathVariable String id,
//                                     @RequestBody Map<String,String> message)
//    {
//            Map<String,String> messageFromDb = getMessage(id);
//            messageFromDb.putAll(message);
//            messageFromDb.put("id",id);
//            return messageFromDb;
//    }
//    @DeleteMapping("{id}")
//    public void delete(@PathVariable String id)
//    {
//        Map<String,String> message = getMessage(id);
//        messages.remove(message);
//    }
    //тестирование
    //fetch('/message/4',{method:'POST',headers:{'Content-Type':'application/json'},
    //body:JSON.stringify({text:'Fourth message(4)',id:10})}).then(res =>console.log(res))
}
