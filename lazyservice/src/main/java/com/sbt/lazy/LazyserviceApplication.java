package com.sbt.lazy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LazyserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LazyserviceApplication.class, args);
	}

}
