package com.sbt.lazy.repos;

import com.sbt.lazy.dao.Address;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AddressRepo extends CrudRepository<Address,Long>{//(Address, Long
}
